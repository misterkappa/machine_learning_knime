# Machine Learning project
## Introduction
The goal of this project is to analyse the IMDB 5000 dataset in order to find if it is possibile to predict the IMDB score of a given movie/TV Series using the available features.
Our findings and conclusions can be read on [this report](IMDB Final relation.pdf), while the complete Knime Workflow can be downloaded from [here](workflows/IMDB_prediction.knwf).
## Useful links:
- Chosen datasets: [IMDB 5000 from Kaggle](https://www.kaggle.com/carolzhangdc/imdb-5000-movie-dataset/version/1)

## Authors:
- [Gian Carlo Milanese](https://www.linkedin.com/in/gian-carlo-milanese-17a054169/) - M.Sc. student at University of Milano Bicocca
- [Khaled Hechmi](https://www.linkedin.com/in/hechmikhaled/) - M.Sc. student at University of Milano Bicocca

## Software used:
- [Knime](https://www.knime.com): 3.7.0
