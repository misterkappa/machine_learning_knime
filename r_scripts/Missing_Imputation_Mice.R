rm(list = ls())
library(dplyr)
library(mice)
library(VIM)

# LOAD DATA
data_path = "dataset/movie_metadata.csv"
data <- read.csv(data_path, stringsAsFactors = F, encoding = 'UTF-8', na.strings=c(""," ","NA"))

# Count missing values per column
data %>% sapply(function(x) sum(is.na(x))) 

# SOURCE: https://datascienceplus.com/imputing-missing-data-with-r-mice-package/

# "As far as categorical variables are concerned, replacing categorical variables
# is usually not advisable. Some common practice include replacing missing 
# categorical variables with the mode of the observed ones, however, it is 
# questionable whether it is a good choice"

# Feature selection (provisional)

str(data)
data <- subset(data, select=-c(movie_title, actor_1_name, actor_2_name,
                               actor_3_name, director_name, genres,
                               plot_keywords, movie_imdb_link, color, 
                               country, language, content_rating, aspect_ratio,
                               title_year))
str(data)

# Count percentage of missing values per column
pMiss <- function(x){sum(is.na(x))/length(x)*100}
apply(data,2,pMiss) %>% sort()

# Pattern of missing data: 3756 complete records, 382 only miss gross, and so on. Not very readable
md.pattern(data)

# Visualization
aggr_plot <- aggr(data, col=c('navyblue','red'), numbers=TRUE, sortVars=TRUE, labels=names(data), cex.axis=.7, gap=3, ylab=c("Histogram of missing data","Pattern"))

# From the source above: "Assuming data is MCAR, 
# too much missing data can be a problem too. 
# Usually a safe maximum threshold is 5% 
# of the total for large datasets. If missing
# data for a certain feature or sample is more
# than 5% then you probably should leave that feature or sample out"

## budget and gross have 9.75% and 17.5% missing values respectively,
## but they seem too important to leave out

### IMPUTATION

str(data)

tempData <- mice(data,m=5,maxit=50,meth='pmm',seed=500)
summary(tempData)

# error: https://www.kaggle.com/c/house-prices-advanced-regression-techniques/discussion/24586
# https://www.rdocumentation.org/packages/mice/versions/3.3.0/topics/mice
# cart: classification and regression trees

tempData <- mice(data,m=5,maxit=50,meth='cart',seed=500) #takes a while
summary(tempData)

# get complete data
completedData <- complete(tempData,1)

# No missing values
completedData %>% sapply(function(x) sum(is.na(x))) 

#visualizations, see source
densityplot(tempData)
stripplot(tempData, pch = 20, cex = 1.2) # takes a while


