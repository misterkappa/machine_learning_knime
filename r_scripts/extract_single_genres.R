#Add to the given dataset new columns corresponding to the genres movies and TV Shows can have
add_genres_columns_to_dataset <- function(df, unique_genres) {
  for (genre in unique_genres) {
    list_of_match_for_current_genre <- grepl(genre, df$genres, ignore.case = T) #The i-th vector element is positive if the i-th element of the genres column contains the current genre value
    list_of_match_for_current_genre <- as.integer(list_of_match_for_current_genre) #Transform the vector in an integer one (1 for TRUE, O for FALSE)
    df[,genre] <- list_of_match_for_current_genre #create a new column, named as the value of the current genre
  }
  return(df)
}

data_path = "dataset/movie_metadata.csv"
data <- read.csv(data_path, stringsAsFactors = F)
head(data)
genres <- data$genres
split_genres <- strsplit(genres,"\\|") # split each word according to | character
split_genres <- unlist(split_genres) # transform split_genres in a vector
unique_genres <- unique(split_genres) # store unique genres value in a vector
enriched_data <- add_genres_columns_to_dataset(data, unique_genres) # enrich the original dataset with new columns named as the genres in unique_genres
head(enriched_data)
#columns_to_show <- c(10,12)
#head(data[,columns_to_show])
#columns_to_show <- c(10,12,28:54)
#head(enriched_data[,columns_to_show])